﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace GetItems
{
    class Program
    {
        public static string Domain = "http://kasasa.qa.liveplatform.com";
        public static string DRI = "/Products";
        public static string Login = "DemoApi@kasasa.LivePlatform.com";
        public static string Password = File.ReadAllText(@"c:\temp\KPassword.txt");

        static void Main(string[] args)
        {

            LivePlatformAuth auth = new LivePlatformAuth(Login, Password, Domain);
            if (auth.Token == null)
            {
                throw new Exception("Login Failed");
            }
            
            // Create a new LivePlatform Cloud and fetch it's items. 
            //LivePlatformCloud cloud = new LivePlatformCloud(Domain, DRI);
            LivePlatformCloud cloud = cloud = new LivePlatformCloud(Domain, DRI, auth); 
            //cloud.getItems();

            LivePlatformQueryFilter filter = new LivePlatformQueryFilter();
            //filter.add("Formatted Name", "=", "KASASA Tunes");
            //filter.add("Event Type", "=", "Email Sent");
            filter.add("Last Update Date", ">", DateTime.UtcNow.AddHours(-1).ToString("o"));
            cloud.findItems(filter);

            // Loop the items in the cloud and print
            foreach (var item in cloud.items)
            {
                String zLine = String.Format("{0} \t |{1}| \t {2}", 
                    item.Tag,
                    item.getStringField("Formatted Name"),
                    item.getStringField("Description"));

                Console.WriteLine(zLine);
            }
            
            Console.ReadLine();
        }
    }
}
