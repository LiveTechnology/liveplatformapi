﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace GetItems
{
    class LivePlatformCloud
    {

        public List<LivePlatformItem> items = new List<LivePlatformItem>();

        public LivePlatformCloud(string Domain, string DRI, LivePlatformAuth auth)
        {
            this.domain = Domain;
            this.DRI = DRI;
            this.auth = auth;
        }


        private LivePlatformAuth auth;
        private string domain;
        private string DRI;
        public string Url
        {
            get
            {
                    return domain + DRI;
            }
        }


        public void getItems()
        {

            String json = string.Empty;
            try
            {
                var client = new WebClient();
                if(auth.Token != null)
                    client.Headers.Add(HttpRequestHeader.Cookie, "LSSID="+auth.Token);
                var fetchURL = this.Url + "/GetItems.do";
                Console.WriteLine(fetchURL);
                json = client.DownloadString(fetchURL);

                LivePlatformGetItems zItems = JsonConvert.DeserializeObject<LivePlatformGetItems>(json);
                items = zItems.Results;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("==============");
                Console.WriteLine(json);
            }
        }


        public void findItems(LivePlatformQueryFilter filter)
        {
            String json = string.Empty;
            var queryString = string.Empty;
            // If we have a filer, get the query string / url friendly version that we'll append to the URL. 
            if (filter != null)
                queryString = filter.getQueryString();

            try
            {
                var client = new WebClient();
                // If we have an auth token, add it to the request header. 
                if (auth.Token != null)
                    client.Headers.Add(HttpRequestHeader.Cookie, "LSSID=" + auth.Token);

                var fetchURL = this.Url + "/FindItems.do?" + queryString;
                Console.WriteLine(fetchURL);
                json = client.DownloadString(fetchURL);

                LivePlatformGetItems zItems = JsonConvert.DeserializeObject<LivePlatformGetItems>(json);
                items = zItems.Results;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("==============");
                Console.WriteLine(json);
            }
        }

    }







}
