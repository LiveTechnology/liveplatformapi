﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace GetItems
{
    class LivePlatformAuth
    {

        string _token;

        public string Token
        {
            get { return _token; }
            set { _token = value; }
        }

        public LivePlatformAuth(String login, String password, string domain)
        {

            Uri loginUri = new Uri(domain + "/login.do");


            using (var wb = new CookieWebClient())
            {
                var data = new NameValueCollection();
                data["Username"] = login;
                data["Password"] = password;
                var response = wb.UploadValues(loginUri, "POST", data);
                string result = System.Text.Encoding.UTF8.GetString(response);
                CookieCollection cookies = wb.CookieContainer.GetCookies(loginUri);
                foreach (Cookie cookieValue in cookies)
                {
                    if (cookieValue.Name == "LSSID")
                    {
                        _token = cookieValue.Value;
                        //Console.WriteLine("Cookie: " + cookieValue.Value);
                    }
                }

            }

        }


        public class CookieWebClient : WebClient
        {
            public CookieContainer CookieContainer { get; private set; }

            /// <summary>
            /// This will instanciate an internal CookieContainer.
            /// </summary>
            public CookieWebClient()
            {
                this.CookieContainer = new CookieContainer();
            }

            /// <summary>
            /// Use this if you want to control the CookieContainer outside this class.
            /// </summary>
            public CookieWebClient(CookieContainer cookieContainer)
            {
                this.CookieContainer = cookieContainer;
            }

            protected override WebRequest GetWebRequest(Uri address)
            {
                var request = base.GetWebRequest(address) as HttpWebRequest;
                if (request == null) return base.GetWebRequest(address);
                request.CookieContainer = CookieContainer;
                return request;
            }


            public string getCookie(string name)
            {
                string value = string.Empty;

                //this.CookieContainer.

                return value;
            }
        }
    }
}
