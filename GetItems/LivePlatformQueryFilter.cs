﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GetItems
{
    class LivePlatformQueryFilter
    {
        List<Tuple<String, String, String>> filterList = new List<Tuple<string, string, string>>();

        public void add(String FieldName, String FilterType, String FilterValue)
        {
            filterList.Add(new Tuple<string, string, string>(FieldName, FilterType, FilterValue));
        }

        public string getQueryString()
        {
            String query = string.Empty;

            foreach (var item in filterList)
            {
                query = query + item.Item1 + item.Item2 + item.Item3 + "&";
            }

            query = query.TrimEnd('&');
            return query;
        }

    }
}
