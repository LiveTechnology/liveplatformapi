﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace GetItems
{
    class LivePlatformItem
    {
        public string Tag;
        public double Rank;

        public LivePlatformItemObject Intersection;
        public LivePlatformItemObject Object;

        /*
        private string name;
        public string Name
        {
            get
            {
                string formattedName = this.Object.Fields.FormattedName;
                string name = this.Object.Fields.Name;
                if (string.IsNullOrEmpty(formattedName))
                {
                    return name;
                }
                else
                {
                    return formattedName;
                }
            }
            set { name = value; }
        }
        */

        public string getStringField(string fieldName)
        {
            String result = string.Empty;
            if (this.Intersection != null)
            {
                result = Intersection.Fields[fieldName];
            } else {
                result = Object.Fields[fieldName];
            }
            return result;
        }

        public class LivePlatformItemObject
        {
            //public LiveStuffItemObjectFields Fields;
            public dynamic Fields;
        }

        /*
        public class LiveStuffItemObjectFields
        {
            public string Name;
            [JsonProperty(PropertyName = "Formatted Name")]
            public string FormattedName;
            public string Description;
            [JsonProperty(PropertyName = "Direct Resource Identifier")]
            public string DRI;
        }
         * */
    }







}
